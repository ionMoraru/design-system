import * as React from 'react';
export interface Props {
    /**
     * Backgroung color
     */
    bg: string;
    children: React.ReactNode;
}
export declare const Button: React.FC<Props>;
