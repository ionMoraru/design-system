const TSDocgenPlugin = require('react-docgen-typescript-webpack-plugin')
module.exports = {
    stories: [
        '../src/**/*.(stories|story).(js|ts|tsx|mdx)',
        '../docs/**/*.(stories|story).(js|ts|tsx|mdx)'
    ],
    addons: [
        '@storybook/addon-knobs/register',
        '@storybook/addon-docs',
        // "@storybook/addon-info"
    ],
    webpackFinal: async config => {
        config.module.rules.push({
            test: /\.(ts|tsx)$/,
            use: [
                {
                    loader: require.resolve('ts-loader'),
                },
                // Optional
                {
                    loader: require.resolve('react-docgen-typescript-loader'),
                },
            ],
        });
        config.plugins.push(new TSDocgenPlugin());
        config.resolve.extensions.push('.ts', '.tsx');
        return config;
    },
};