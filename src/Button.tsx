import * as React from 'react';


export interface Props {
    /**
     * Backgroung color
     */
    bg: string
    children: React.ReactNode
}

export const Button: React.FC<Props> = ({ bg, children }) => {
    return <button style={{ background: bg }}>{children}</button>
} 

