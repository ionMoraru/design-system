import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { withKnobs, color, text } from '@storybook/addon-knobs/react';
// import { withInfo } from '@storybook/addon-info';
import { Button } from './Button.tsx';

export default {
  title: 'COMPONENTS|Button',
  decorators: [withKnobs], // , withInfo
  parameters: {
    info: { inline: true },
  },
};

export const withText = () => <Button bg={color('bg', 'tomato', 'group1')}>Hello Button</Button>;

export const withEmoji = () => (
  <Button bg={text('bg', 'yellow')}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);
